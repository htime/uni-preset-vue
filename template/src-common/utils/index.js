export function validateFormData(formData, requiredOption) {
  return new Promise(resolve => {
    let isValid = true
    let message = ''
    if (requiredOption && requiredOption?.length) {
      requiredOption.some(option => {
        const val = formData[option['key']]
        const isArray = Array.isArray(val)
        const typeVal = typeof val
        const isEmpty = val === null || typeVal === 'undefined' || (typeVal === 'string' && val.trim() === '') || (isArray && val.length === 0)
        if (isEmpty) {
          isValid = false
          message = option['message']
          return true
        }
        return false
      })
    }
    if (!isValid) {
      uni.$u.toast(message)
    }
    resolve(isValid)
  })
}

export function formatTimeStr(date) {
  date = date instanceof Date ? date : new Date(date)
  const myYear = date.getFullYear()
  const myMonth = (date.getMonth() + 1).toString().padStart(2, '0')
  const myDate = date.getDate().toString().padStart(2, '0')
  const myHours = date.getHours().toString().padStart(2, '0')
  const myMinutes = date.getMinutes().toString().padStart(2, '0')
  const mySeconds = date.getSeconds().toString().padStart(2, '0')
  return `${myYear}-${myMonth}-${myDate} ${myHours}:${myMinutes}`
}

export function arrayDistinct(arr) {
  const s1 = new Set(arr)
  return Array.from(s1)
}

export function twoBaseArrayHasRepeat(arr1, arr2) {
  const len1 = arr1.length + arr2.length
  const len2 = arrayDistinct([...arr1, ...arr2]).length
  return len2 < len1
}

export const generateId = function () {
  return Math.floor(Math.random() * 10000)
}

export const obj2Params = function (obj) {
  let str = '?'
  const keys = Object.keys(obj)
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i]
    str += `${key}=${obj[key]}${i === keys.length - 1 ? '' : '&'}`
  }
  return str
}

export function getTimeDisWithCurrentTime(direct_time, end_times) {
  // direct_time格式为yyyy-mm-dd hh:mm:ss 指定时间
  var now_time = Date.parse(new Date(direct_time)) // 当前时间的时间戳
  var end_time = Date.parse(new Date(end_times)) // 指定时间的时间戳
  if (end_time < now_time) {
    //  截止时间已过
    return false
  } else {
    // 计算相差天数
    var time_dis = end_time - now_time
    var days = Math.floor(time_dis / (24 * 3600 * 1000))
    // 计算出小时数
    var leave1 = time_dis % (24 * 3600 * 1000) // 计算天数后剩余的毫秒数
    var hours = Math.floor(leave1 / (3600 * 1000))
    // 计算相差分钟数
    var leave2 = leave1 % (3600 * 1000) // 计算小时数后剩余的毫秒数
    var minutes = Math.floor(leave2 / (60 * 1000))
    // 计算相差秒数
    var leave3 = leave2 % (60 * 1000) // 计算小时数后剩余的毫秒数
    var second = leave3 / 1000
    return `${days > 0 ? days + '天' : ''}${hours > 0 ? hours + '时' : ''}${minutes > 0 ? minutes + '分' : ''}`
  }
}

/**
 * 对象数组去重合并，根据key值判断唯一
 * @param {*} arr1
 * @param {*} arr2
 * @param {*} key
 */
export function objArrayDistinctMerge(arr1 = [], arr2 = [], key) {
  const objMap = new Map()
  const rawArr = [].concat(arr1).concat(arr2)
  rawArr.forEach(i => objMap.set(i[key], i))
  const mergeArr = []
  objMap.forEach(value => mergeArr.push(value))
  return mergeArr
}
