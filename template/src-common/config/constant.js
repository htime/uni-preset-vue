export const refreshTokenUrl = '/auth/refreshToken'
export const loginUrl = '/auth/admin'
export const systemConfigUrl = '/common/listSystemConfig'
export const logoutUrl = '/auth/logout'
export const userinfoUrl = '/system/getLoginInfo'
export const loginPageUrl = '/pages/login/index'

// 账号权限相关
export const whiteList = [loginUrl]
export const authList = [loginUrl, '/auth/logout', refreshTokenUrl]
export const ADMIN_ROLE = ['system', 'center']
export const MAINTENANCE_ROLE = ['system', 'maintenance']

// 地图相关
export const MAP_TYPE = 'gcj02' // gcj02 || wgs84
export const TENCENT_MAP_KEY = '' // 腾讯地图 - 我的应用的key
export const TENCENT_MAP_SECRET = '' // 腾讯地图 - 编辑我的应用 - 启用产品WebServiceAPI 选择"签名校验"时的 Secret key

// 系统主题色
export const mainColor = '#375df6'
