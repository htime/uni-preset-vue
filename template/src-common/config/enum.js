/**
 * 动态表单类型
 * 1文本 2数字 3日期时间 4单选 5文件上传 6图片 7日期区间
 */
export const dynamicType = {
  1: 'input',
  2: 'number',
  3: 'datetime',
  4: 'select',
  5: 'upload-file',
  6: 'upload-image',
  7: 'daterange'
}
export const dynamicPlaceholder = {
  1: '请输入',
  2: '请输入',
  3: '请选择',
  4: '请选择',
  5: '请上传',
  6: '请上传',
  7: '请选择'
}
