const getters = {
  role: state => state.permission.role,
  rolename: state => state.permission.rolename,
  name: state => state.permission.userinfo.name,
  id: state => state.permission.userinfo.id,
  userinfo: state => state.permission.userinfo
}
export default getters
