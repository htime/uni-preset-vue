import Vue from 'vue'
import App from './App'
import './uni.promisify.adaptor'
import uView from 'uview-ui'
import '@/styles/main.scss'
import { mainColor } from '@/config/constant'
import store from '@/store'
import DlEmptyState from '@/components/DlEmptyState'

Vue.component('DlEmptyState', DlEmptyState)
Vue.config.productionTip = false
Vue.prototype.$store = store
Vue.use(uView)

uni.$u.setConfig({
  config: {
    unit: 'rpx'
  },
  props: {
    line: {
      color: '#eeeeee',
      hairline: false
    },
    upload: {
      width: '160rpx',
      height: '160rpx',
      uploadIcon: 'plus',
      uploadText: '添加图片',
      uploadIconColor: '#000000'
    },
    empty: {
      textSize: '30rpx',
      textColor: '#333333'
    },
    loadingPage: {
      bgColor: 'rgba(0,0,0,0.3)',
      loadingText: '数据加载中',
      loadingColor: '#ffffff',
      color: '#ffffff',
      fontSize: '32rpx'
    },
    notify: {
      fontSize: 30,
      duration: 1000
    },
    checkbox: {
      labelSize: '32',
      iconSize: '32'
    },
    icon: {
      size: '32'
    },
    badge: {
      showZero: true
    },
    button: {
      size: 'large'
    },
    tabs: {
      activeStyle: {
        color: mainColor
      }
    }
  },
  color: {
    primary: mainColor
  }
})
const app = new Vue({
  store,
  ...App
})
app.$mount()
