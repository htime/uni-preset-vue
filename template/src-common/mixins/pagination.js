export default {
  data() {
    return {
      initing: false, // 初始化
      current: 0, // 当前页面多个tabs时，当前选中的tab
      paginationData: {
        page: 1,
        size: 5,
        total: 0
      },
      tableData: []
    }
  },
  // 触底时触发
  onReachBottom() {
    if (!this.initing) {
      if (this.tableData.length < this.paginationData.total) {
        this.paginationData.page++
        this.getTableData()
      }
    }
  },
  // 下拉刷新时触发
  onPullDownRefresh() {
    this.paginationData.page = 1
    this.initData && this.initData()
  },
  methods: {
    // 切换tab搜索或点击搜索时触发
    confirmSearch(val) {
      this.paginationData.page = 1
      this.current = val?.index
      this.getTableData()
    },
    // 搜索条件变化时触发
    changeSearch() {
      this.paginationData.page = 1
      uni.$u.debounce(this.getTableData, 500)
    }
  }
}
