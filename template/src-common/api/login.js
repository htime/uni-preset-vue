import HttpRequest from '@/utils/http/request'
import { refreshTokenUrl, loginUrl, userinfoUrl, logoutUrl, systemConfigUrl } from '@/config/constant'

// 账号登录
export function login(params) {
  return HttpRequest.post(loginUrl, params)
}

// 账号退出登录
export function logout() {
  return HttpRequest.post(logoutUrl)
}

// 获取账号详情
export function getUserInfo() {
  return HttpRequest.post(userinfoUrl)
}

// 刷新token
export function refreshAuth() {
  return HttpRequest.post(refreshTokenUrl)
}

// 获取系统配置
export function getSystemConfig() {
  return HttpRequest.post(systemConfigUrl)
}
