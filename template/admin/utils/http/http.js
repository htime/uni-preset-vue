/**
 * 封装请求
 */
// 引入 uni-ajax 模块
import ajax from 'uni-ajax'
import { HTTP_STATUS, SYSTEM_CODE } from './http-status'
import { getSignature } from '../signature'
import { whiteList, authList, loginPageUrl } from '@/config/constant'
import { getToken, removeToken } from '@/utils/auth'

const APPID = process.env.VUE_APP_ID
const baseUrl = process.env.VUE_APP_BASE_URL
const baseApi = process.env.VUE_APP_BASE_API

// 创建请求实例
const instance = ajax.create({
  baseURL: baseUrl + baseApi
})
const pending = new Promise(() => {})

// 添加请求拦截器
instance.interceptors.request.use(
  config => {
    const url = config.url
    if (authList.indexOf(url) > -1) {
      config.url = baseUrl + url
    }
    const token = getToken()
    if (whiteList.indexOf(url) === -1) {
      !!token && (config.header['Authorization'] = 'Bearer ' + token)
    }
    const timestamp = Date.parse(new Date()) / 1000
    if (config.data) {
      config.data.appid = APPID
      config.data.timestamp = timestamp
    }
    const config_ = JSON.parse(JSON.stringify(config.data))
    const sign = getSignature(config.url, config_)
    config.params = extend(config.params || {}, { sign, timestamp })
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

function extend(to, _from) {
  for (const key in _from) {
    to[key] = _from[key]
  }
  return to
}

// 添加响应拦截器 如果请求得到的响应转态码不是2开头的,就会主动抛出错误
instance.interceptors.response.use(
  response => {
    // 对响应数据做些什么
    const { code, message } = response.data
    if (code === SYSTEM_CODE.SUCCESS) {
      return response.data
    } else if (code === SYSTEM_CODE.UNAUTHORIZED) {
      removeToken()
      uni.reLaunch({ url: loginPageUrl })
      return pending
    } else {
      uni.showToast({ icon: 'fail', title: message })
      return response.config.catch ? pending : Promise.reject(new Error(message))
    }
  },
  error => {
    let errorMsg = ''
    if (error.response) {
      // 请求已发出，且服务器的响应状态码超出了 2xx 范围
      const { statusCode } = error.response
      if (statusCode === HTTP_STATUS.NOT_FOUND) {
        errorMsg = '请求资源不存在'
      } else if (statusCode === HTTP_STATUS.BAD_GATEWAY) {
        errorMsg = '服务端出现了问题'
      } else if (statusCode === HTTP_STATUS.FORBIDDEN) {
        errorMsg = '没有权限访问' // 退出登录
        removeToken()
        uni.reLaunch({ url: loginPageUrl })
      } else if (statusCode === HTTP_STATUS.AUTHENTICATE) {
        errorMsg = '需要鉴权' // 退出登录
        removeToken()
        uni.reLaunch({ url: loginPageUrl })
      }
    } else if (error.request) {
      errorMsg = '服务器没有响应' // 请求已发出，但没有接收到任何响应  在浏览器中，error.request 是 XMLHttpRequest 实例  在 node.js 中，error.request 是 http.ClientRequest 实例
    } else {
      errorMsg = error.message || error.errMsg // 引发请求错误的错误信息
    }
    uni.showToast({
      icon: 'error',
      title: errorMsg
    })
    const isCatch = error.response ? error.response.config.catch : error.config.catch
    return isCatch ? pending : Promise.reject(new Error(errorMsg)) // 对响应错误做些什么
  }
)

// 导出 create 创建后的实例
export default instance
