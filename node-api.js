const glob = require('glob')
const path = require('path')

const dir = path.resolve(__dirname, './template/src-common')
const fs = glob.sync('**/*', { cwd: dir, nodir: false })

fs.forEach(rawPath => {
  const sourcePath = path.resolve(dir, rawPath)
  console.log(rawPath + ' === ' + sourcePath)
})
